#!/bin/bash

# Nombre del archivo package.json
package_json="package.json"

# Comprobar si el archivo package.json existe
if [ ! -f "$package_json" ]; then
  echo "Error: El archivo $package_json no existe en este directorio."
  exit 1
fi

# Scripts a agregar
scripts_to_add='
{
  "yamllint:dev": "yamllint openshift/dev",
  "yamllint:pre": "yamllint openshift/pre",
  "yamllint:pro": "yamllint openshift/pro",
  "sonar:scanner": "sonar-scanner",
  "dependency:check": "dependency-check.sh --project dep-webapp --scan webapp.war --out dep-webapp.html;",
  "jmeter:scan": "jmeter -n"
}
'

# Extraer la sección "scripts" actual del archivo package.json
current_scripts=$(grep -oP '(?<=^  "scripts": \{).*(?=\n  \},)' "$package_json")

# Fusionar los scripts existentes con los nuevos scripts
merged_scripts="$current_scripts$scripts_to_add"

# Actualizar el archivo package.json con los scripts fusionados utilizando sed
sed -i "/\"scripts\": {/s/.*/&$merged_scripts,/" "$package_json"

echo "Los scripts se han agregado con éxito a $package_json."
