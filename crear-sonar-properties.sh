#!/bin/bash

# Nombre del archivo de propiedades de SonarQube
sonar_properties="sonar-project.properties"

# Pedir al usuario los valores de project_key, project_name y project_token
echo "Ingrese el valor para project_key:"
read project_key

echo "Ingrese el valor para project_name:"
read project_name

echo "Ingrese el valor para project_token:"
read project_token

# Contenido del archivo sonar-project.properties
properties_content="sonar.projectKey=$project_key
sonar.projectName=$project_name
sonar.login=$project_token
sonar.host.url=http://igstkwndk01.cl.bsch:8080/
sonar.projectVersion=1.0
sonar.sourceEncoding=UTF-8
sonar.language=js,ts
sonar.verbose=true
sonar.sources=src
sonar.tests=src
sonar.javascript.lcov.reportPaths=coverage/lcov.info
sonar.testExecutionReportsPaths=reports/ut_report.xml
sonar.test.inclusions=**/*.spec.ts
"

# Crear el archivo sonar-project.properties
echo "$properties_content" > "$sonar_properties"

echo "El archivo $sonar_properties ha sido creado con éxito."
